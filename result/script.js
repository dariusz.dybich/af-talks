var eurExchangeRate = null;
var url = 'http://api.nbp.pl/api/exchangerates/rates/A/EUR/?format=json';

// var Http = new XMLHttpRequest();
// Http.open('GET', url);
// Http.send();

// Http.onreadystatechange = function() {
//   if (this.readyState === 4 && this.status === 200) {
//     var response = JSON.parse(Http.responseText);
//     eurExchangeRate = response.rates[0].mid;
//   }
// }

fetch(url)
  .then(resp => resp.json())
  .then(resp => eurExchangeRate = resp.rates[0].mid);

document.getElementById('calculate-btn').addEventListener('click', calculate);

function calculate() {
  var pln = (+document.getElementById('pln-value').value).toFixed(2);
  var eur = pln / eurExchangeRate;
  document.getElementById('result-pln').innerHTML = pln;
  document.getElementById('result-eur').innerHTML = eur.toFixed(2);
  var resultElement = document.getElementById('result');
  resultElement.style.display = 'block';
  var noResultElement = document.getElementById('no-result');
  noResultElement.style.display = 'none';
}

document.getElementById('pln-value').addEventListener('input', onValueChange);

function onValueChange(event) {
  var calculateBtn = document.getElementById('calculate-btn');
  
  if (event.target.value && +event.target.value > 0) {
    calculateBtn.removeAttribute('disabled');
  }
  else {
    calculateBtn.setAttribute('disabled', true);
  }
}
